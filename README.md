# pgclient

Alpine Linux Docker Image with preinstalled packages:

- [psql](https://www.postgresql.org/docs/current/app-psql.html)
- [pgBackRest](https://pgbackrest.org/)
