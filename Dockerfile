FROM docker.io/library/alpine

LABEL org.opencontainers.image.title="zwieratko PostgreSQL 17 client"
LABEL org.opencontainers.image.authors="zwieratko1@gmail.com"
LABEL org.opencontainers.image.source="https://gitlab.com/docker-images6383311/pgclient"
LABEL org.opencontainers.image.licenses="MIT"

WORKDIR /tmp

RUN <<EOF
apk update
apk upgrade
apk add postgresql17-client pgbackrest
rm -rf /var/cache/apk/*
EOF

CMD ["/bin/sh"]
